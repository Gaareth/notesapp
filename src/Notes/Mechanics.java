package Notes;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LookAndFeel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.SwingConstants;
import javax.swing.JPasswordField;




/**
 * 
 * @author Gareth
 * @since 06.05.18
 * created 02.05.18
 * 
 * 
 * needed time for finishing: 36h // not working time xD
 *
 */








public class Mechanics extends JFrame {

	private LookAndFeel defaultLook = UIManager.getLookAndFeel();

	private ArrayList<Notiz> notizenList = new ArrayList<>();
	private ArrayList<JLabel> labelList = new ArrayList<>();

	private DateFormat currentDate =  DateFormat.getDateInstance(DateFormat.SHORT);  
	private GregorianCalendar calendar = new GregorianCalendar();
	SimpleDateFormat formatTime;

	public JPanel contentPane = new JPanel();
	private JLabel[] label = new JLabel[500];
	private JLabel[] labelAddText = new JLabel[500];
	private JLabel[] labelDate = new JLabel[500];
	private JPanel[] notesText = new JPanel[500];
	private JSeparator[] separator = new JSeparator[500];
	private JPanel panel;
	private JLabel lblAnzahlNotizen;
	private JLabel lblNotizen;
	private JLabel lblAdd;
	private JSeparator separator_2;
	private JLabel lblBack;
	private JTextArea[] notesTextArea = new JTextArea[500];
	private JTextPane notesTextArea2 = new JTextPane();
	private JLabel lblDelete;
	private JSeparator separator_3;
	private JButton btnAdd;
	private JLabel lblSettings;

	private JLabel lblOrangeBackIcon;
	private ImageIcon orangeBackIcon = new ImageIcon((Mechanics.class.getResource("/pictures/backOrangeIcon23x.png")));
	private ImageIcon yellowBackIcon = new ImageIcon((Mechanics.class.getResource("/pictures/backYellowIcon23x.png")));

	private JTextField textField = new JTextField();
	private CardLayout cl = new CardLayout();
	
	
	private JPanel startPanel = new JPanel(null);
	
	
	private int i = 0;
	private JPanel notizenPanel = new JPanel(null);
	private JButton btnFertig;
	private JButton btnBearbeiten;
	private JButton btnBearbeiten_1;
	
	
	private File fileLocalNotes = new File(System.getProperty("user.home")+"/.notes./NotesConfig.json");
	private File fileCloudNotes = new File(System.getProperty("user.home")+"/.notes./NotesCloud.json");

	private JScrollPane scrollPane;

	private int besonderesI;
	private JLabel lblByGareth;
	private JLabel lblDate;
	private JLabel lblAdd_Doc;
	private JPanel panel_Settings;
	private JPanel panel_1;
	private JPanel panelContactPanel;
	private JSeparator separator_1;
	private boolean extended_Settings = false;
	private JPanel fakePanel;
	private JLabel settings_Color;
	private JLabel settings_FontSize;
	private JPanel selectionPanel;
	private JLabel lblSelection;
	private JLabel lblFolders;
	private JButton lblLocal;
	private JButton lblCloud;
	private JPanel panel_2;
	private JSeparator separator_4;
	private JSeparator separator_5;
	private JSeparator separator_6;


	private JPanel panel_LocalNotes;
	private JLabel lblNotes_1;
	private JSeparator separator_7;
	private JPanel panel_CloudNotes;
	private JLabel lblNotes;
	
	
	private String cloudJson;
	
	public Mechanics() {
		
		contentPane.setLayout(cl);
		startPanel.setLayout(null);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		initNotesTextComps();
		initStartPanel();
		initSelectionPanel();
		
		
		addStartPanelComps();

		addNotesPanelComps();
		
		initSettingsPanel();
		
		//addFakePanelComps(); //TODO: Delete maybe
		
		setWindowsLook(); 	
		
		JPanel panelScroll = createContactPanel();
	    JScrollPane sp = new JScrollPane();
	    sp.setBorder(null);
	    sp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    sp.setViewportView(panelScroll);
	    
	   
		sp.setPreferredSize(new Dimension(341, 375));
		
		setDefaultLook();
		
		panel_2 = new JPanel();
		panel_2.setBounds(10, 50, 342, 381);
		startPanel.add(panel_2);
		
		contentPane.setPreferredSize(new Dimension(361, 505));

		panel_2.add(sp);

		separator_1 = new JSeparator();
		separator_1.setBounds(10, 49, 341, 8);
		startPanel.add(separator_1);
		
		contentPane.add(startPanel, "startPanel");	
		
		
		contentPane.add(notizenPanel, "notizenPanel");
		contentPane.add(selectionPanel, "selectionPanel");
		selectionPanel.setLayout(null);
		
		
		
		
		
		

		scrollPane.setViewportView(notesTextArea2);
		
		
		

		
		cl.show(contentPane, "startPanel");
		


		checkForNotes(fileLocalNotes, false);  //looks if there is any content in json and adds it to notes app
		setPreferredSize();
		

		
	}
	public void initSelectionPanel() {
		selectionPanel = new JPanel();
		
		lblSelection = new JLabel(orangeBackIcon);
		lblSelection.setBounds(-11, 222, 32, 32);
		startPanel.add(lblSelection);
		
		lblSelection.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				
				cl.show(contentPane, "selectionPanel");
			}
		});

		
		lblFolders = new JLabel("Folders");
		lblFolders.setFont(new Font("Ebrima", Font.BOLD, 25));
		lblFolders.setBounds(15, 0, 108, 36);
		selectionPanel.add(lblFolders);
		
		lblLocal = new JButton("local");
		lblLocal.setHorizontalAlignment(SwingConstants.LEFT);
		lblLocal.setBorder(null);
		lblLocal.setFocusPainted(false);
		lblLocal.setRolloverEnabled(false);
		lblLocal.setContentAreaFilled(false);
		lblLocal.setBounds(15, 63, 326, 14);
		lblLocal.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				clearNotesList();
				removeAllNote();
				checkForNotes(fileLocalNotes, false);
				cl.show(contentPane, "startPanel");				
			}
		});
		selectionPanel.add(lblLocal);
		
		lblCloud = new JButton("cloud");
		lblCloud.setHorizontalAlignment(SwingConstants.LEFT);
		lblCloud.setBorder(null);
		lblCloud.setFocusPainted(false);
		lblCloud.setRolloverEnabled(false);
		lblCloud.setContentAreaFilled(false);
		lblCloud.setBounds(15, 112, 326, 14);
		lblCloud.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				clearNotesList();
				removeAllNote();
				checkForNotes(fileCloudNotes, true);
				cl.show(contentPane, "startPanel");
			}
		});
		selectionPanel.add(lblCloud);
		
		separator_4 = new JSeparator();
		separator_4.setBounds(0, 76, 351, 7);
		selectionPanel.add(separator_4);
		
		separator_5 = new JSeparator();
		separator_5.setBounds(0, 124, 351, 2);
		selectionPanel.add(separator_5);
		
		separator_6 = new JSeparator();
		separator_6.setBounds(0, 100, 351, 7);
		selectionPanel.add(separator_6);
		
		separator_7 = new JSeparator();
		separator_7.setBounds(0, 150, 351, 7);
		selectionPanel.add(separator_7);
		
		
		panel_LocalNotes = new JPanel();
		panel_LocalNotes.setBounds(0, 75, 351, 25);
		selectionPanel.add(panel_LocalNotes);
		panel_LocalNotes.setLayout(null);
		panel_LocalNotes.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				panel_LocalNotes.setBackground(new Color(255, 140, 0));
				separator_4.setForeground(new Color(255, 140, 0));
				separator_6.setForeground(new Color(255, 140, 0));


				super.mouseClicked(e);
				System.out.println("XDDDDDDDDDDDDDDDDDD");
			}
		});
		
		lblNotes_1 = new JLabel("Notes");
		lblNotes_1.setBounds(15, 5, 46, 14);
		panel_LocalNotes.add(lblNotes_1);
		
		panel_CloudNotes = new JPanel();
		panel_CloudNotes.setBounds(0, 124, 351, 23);
		selectionPanel.add(panel_CloudNotes);
		panel_CloudNotes.setLayout(null);
		
		lblNotes = new JLabel("Notes");
		lblNotes.setBounds(15, 5, 40, 14);
		panel_CloudNotes.add(lblNotes);
		
		
		
		
		
		
		
		
	}
	public JPanel createContactPanel()  //TODO: Contact Panel
	{
	      panelContactPanel = new JPanel();
	      panelContactPanel.setLayout(null);
	      
	      setDefaultLook();
		  setWindowsLook();

	      
	      
	     
	      panelContactPanel.setPreferredSize(new Dimension(309, (notizenList.size() * 45)) );
	 
	      return panelContactPanel;
	}
	
	public void setPreferredSize()
	{
	    panelContactPanel.setPreferredSize(new Dimension(309, (notizenList.size() * 45)) );
		
	}
	public void initSettingsPanel()
	{
		settings_Color = new JLabel("color");
		settings_Color.setBounds(75, 60, 46, 14);
		settings_Color.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				changeColorOfPane(notesTextArea2, Color.RED);
				

				
			}
		});
		
		settings_FontSize = new JLabel("size");
		settings_FontSize.setBounds(150, 60, 46, 14);
		settings_FontSize.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				notesTextArea2.setFont(new Font(notesTextArea2.getFont().getFontName(), 0, 30));
				notizenList.get(besonderesI).setSize(30);
				System.out.println("Changed Size to 30 in Note: " + besonderesI);
				save();
			}
		});
		
		panel_Settings.add(settings_FontSize);
		panel_Settings.add(settings_Color);
	}
	public void initStartPanel()
	{
		setWindowsLook();
		
		scrollPane = new JScrollPane();
		scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		scrollPane.setBounds(0, 47, 351, 346);
		
		setDefaultLook();
		
		
		
		fakePanel = new JPanel(null);
		fakePanel.setBounds(400, 0, 361, 505);
		

		
		
		panel = new JPanel();

		
	
		separator_2 = new JSeparator();
		
		lblAdd = new JLabel("");
		lblAdd.setIcon(new ImageIcon(Mechanics.class.getResource("/pictures/addIconOrange2.png")));
		lblNotizen = new JLabel("Notizen");
		lblNotizen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cl.show(contentPane, "selectionPanel");
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNotizen.setForeground(Color.ORANGE);
				super.mouseEntered(e);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lblNotizen.setForeground(new Color(0, 0, 0));
				
				super.mouseExited(e);
			}
		});
		lblAnzahlNotizen = new JLabel(notizenList.size() + " Notizen");
		
		separator_3.setBounds(0, 440, 351, 7);
		lblAdd.setBounds(303, 450, 32, 32);
		separator_2.setBounds(0, 440, 361, 14);
		panel.setBounds(0, 47, 351, 392);
		lblNotizen.setBounds(15, 0, 108, 36);
		lblAnzahlNotizen.setBounds(361 / 2 - 50, 455, 100, 14);

		lblAdd.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNotizen.setFont(new Font("Ebrima", Font.BOLD, 25));
		lblAnzahlNotizen.setFont(new Font("Monospaced", Font.PLAIN, 13));
		
		panel.setLayout(null);
		
		lblByGareth = new JLabel("by Gareth");
		lblByGareth.setFont(UIManager.getFont("CheckBoxMenuItem.acceleratorFont"));
		lblByGareth.setBounds(0, 482, 50, 14);
		
		btnBearbeiten = new JButton("Bearbeiten");
		btnBearbeiten.setContentAreaFilled(false);
		btnBearbeiten.setBackground(new Color(240, 240, 240));
		btnBearbeiten.setFont(new Font("Ebrima", Font.BOLD, 15));
		btnBearbeiten.setBorderPainted(false);
		btnBearbeiten.setRolloverEnabled(false);
		btnBearbeiten.setFocusPainted(false);
		btnBearbeiten.setForeground(new Color(255, 140, 0));
		btnBearbeiten.setBounds(237, 0, 114, 36);
		btnBearbeiten.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				
				btnBearbeiten.setForeground(new Color(250, 200, 0));

				
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				
				btnBearbeiten.setForeground(new Color(250, 140, 0));

			}
		});

		
		
		
		btnAdd = new JButton("");
		btnAdd.setBorder(null);
		btnAdd.setFocusPainted(false);
		btnAdd.setRolloverEnabled(false);
		btnAdd.setContentAreaFilled(false);
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				btnAdd.setIcon(new ImageIcon(Mechanics.class.getResource("/pictures/addIconYellow.png")));

				
				
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				
				btnAdd.setIcon(new ImageIcon(Mechanics.class.getResource("/pictures/addIconOrange3.png")));

			}
		});
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPreferredSize();
				startPanel.setVisible(false);
				startPanel.updateUI();
				addFakePanelComps();
				i = notizenList.size();
				
				formatTime = new SimpleDateFormat("HH:mm:ss");
				
				notizenList.add(new Notiz("Neue Notiz", currentDate.format(calendar.getTime()), formatTime.format(new Date()),i, 12, ""));
				printList();
				addLabel(notizenList.get(i).getText(), i);
				lblAnzahlNotizen.setText(notizenList.size() + " Notizen");
				panel.updateUI();
				System.out.println(notizenList.get(i).getTime());
				
				lblDate.setText(notizenList.get(i).getDate() + " --- " +notizenList.get(i).getTime());

				besonderesI = i;
				
				new javax.swing.Timer(1, new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						fakePanel.setLocation(fakePanel.getX() - 2, 0);
				
						startPanel.updateUI();
						contentPane.updateUI();

						 System.out.println(fakePanel.getX());
		                  if (fakePanel.getLocation().getX() < 10) 
		                  {
		                	  addNotesPanelComps();
		                	  
		                     ((javax.swing.Timer) e.getSource()).stop();
		                     System.out.println("Timer stopped");
		                     switchPanel(i);
		                     fakePanel.setBounds(400, 0, 361, 505);

			            }	
							
					}
				}).start();
				
				
			}
		});
		btnAdd.setIcon(new ImageIcon(Mechanics.class.getResource("/pictures/addIconOrange3.png")));
		btnAdd.setBounds(303, 450, 40, 40);
		
	

		
	}
	public void addStartPanelComps()
	{
		//startPanel.add(lblAdd);
		startPanel.add(lblNotizen);
		startPanel.add(lblAnzahlNotizen);
		//startPanel.add(panel);
		startPanel.add(separator_2);
		startPanel.add(btnAdd);
		startPanel.add(lblByGareth);
		startPanel.add(fakePanel);
		//startPanel.add(btnBearbeiten);
	}
	public void addNotesPanelComps()
	{
		notizenPanel.add(lblBack);
  		//notizenPanel.add(notesTextArea2);
  		notizenPanel.add(lblOrangeBackIcon);
  		notizenPanel.add(btnFertig);
  		notizenPanel.add(scrollPane);
  		notizenPanel.add(lblDelete);
  		notizenPanel.add(separator_3);
  		notizenPanel.add(lblDate);
  		notizenPanel.add(lblSettings);
  		notizenPanel.add(lblAdd_Doc);
  		notizenPanel.add(panel_Settings);
	}
	public void addFakePanelComps()
	{
		fakePanel.add(lblBack);
		fakePanel.add(btnFertig);
		fakePanel.add(lblOrangeBackIcon);
		fakePanel.add(scrollPane);
		fakePanel.add(lblDelete);
		fakePanel.add(separator_3);
		fakePanel.add(lblDate);
		fakePanel.add(lblSettings);
		fakePanel.add(lblAdd_Doc);
		fakePanel.add(panel_Settings);

	}
	public void initNotesTextComps()
	{
		
		setWindowsLook();
		
		scrollPane = new JScrollPane();
		scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		scrollPane.setBounds(0, 47, 351, 346);
		
		setDefaultLook();
		
		Component comp = SwingUtilities.getRoot(contentPane);
		lblBack = new JLabel("Notizen");
		lblBack.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblOrangeBackIcon = new JLabel();
		lblOrangeBackIcon.setIcon(orangeBackIcon);
		lblOrangeBackIcon.setBounds(0, 10, 23, 23);

		separator_3 = new JSeparator();
		separator_3.setForeground(Color.GRAY);
		
		lblDate = new JLabel();
		lblDate.setBounds(361 /2 - (140 / 2), 26, 127, 14);

		panel_Settings = new JPanel();
		panel_Settings.setBackground(Color.LIGHT_GRAY);
		panel_Settings.setBounds(0, 439, 351, 0);
		panel_Settings.setLayout(null);

		lblSettings = new JLabel("settings");
		lblSettings.setBounds(78, 450, 32, 32);
		lblSettings.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				super.mouseClicked(arg0);
				
				settingsAnimation();

				
			}
			
		});
		
		lblAdd_Doc = new JLabel("add");
		lblAdd_Doc.setBounds(153, 450, 32, 32);
	
		lblBack.setForeground(new Color(255,140,0));
		lblBack.setBounds(20, 5, 100, 35);
		switchToStartPanel(lblBack, "Back");
		switchToStartPanel(lblOrangeBackIcon, "Back");
		
		
		
		notesTextArea2 = new JTextPane();
		notesTextArea2.setBounds(0, 47, 351, 374);
		notesTextArea2.setBackground(new Color(240, 240, 240));
	
		btnFertig = new JButton("Fertig");
		btnFertig.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				btnFertig.setForeground(new Color(250, 200, 0));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btnFertig.setForeground(new Color(250, 140, 0));
			}
		});
		btnFertig.setContentAreaFilled(false);
		btnFertig.setFocusPainted(false);
		btnFertig.setFont(new Font("Ebrima", Font.BOLD, 20));
		btnFertig.setRolloverEnabled(false);
		btnFertig.setBorderPainted(false);
		btnFertig.setBackground(new Color(240, 240, 240));
		btnFertig.setForeground(new Color(255, 140, 0));
		btnFertig.setBounds(252, 6, 89, 30);
		btnFertig.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				System.out.println(notesTextArea2.getText());
				
				String str = notesTextArea2.getText();
				if(notesTextArea2.getText().isEmpty())
				{
					notizenList.get(besonderesI).setText("Neue Notiz");
				}else
				{
					notizenList.get(besonderesI).setText(str);
				}
				int ix = i-1;
				System.out.println("text nummer " + besonderesI + " gespeichert");
				
				switchToStartPanel2(lblBack, "Back", besonderesI);
				switchToStartPanel2(lblOrangeBackIcon, "Back", besonderesI);
				lblDate.setText(notizenList.get(besonderesI).getDate() + " --- " +notizenList.get(besonderesI).getTime());
				
				updateLabelAddText(besonderesI);

				save();
			}
		});
		
		
		lblDelete = new JLabel("");
		lblDelete.addMouseListener(new MouseAdapter() { //Delete Note
			@Override
			public void mouseClicked(MouseEvent e) {
				
				setPreferredSize();
				removeNote(besonderesI);
				startPanel.add(lblNotizen);
				checkForNotes(fileLocalNotes, false);
				
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				
				lblDelete.setIcon(new ImageIcon(Mechanics.class.getResource("/pictures/deleteYellowIcon23x.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblDelete.setIcon(new ImageIcon(Mechanics.class.getResource("/pictures/deleteOrangeIcon32(2)x.png")));

			}
		});
		lblDelete.setIcon(new ImageIcon(Mechanics.class.getResource("/pictures/deleteOrangeIcon32(2)x.png")));
		lblDelete.setBounds(10, 450, 32, 32);
		
		
	}
	public void switchPanel(int i)
	  {
	    this.cl.show(this.contentPane, "notizenPanel");
	    this.notesTextArea2.setText(null);
	  }
	
	
	public void removeAllNote()
	{
		try
		{
			panelContactPanel.removeAll();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	
	}
	public void removeNote(int i)
	{
		cl.show(contentPane, "startPanel");
		
		panel.remove(separator[i]); //Trying to remove separator and other ui components
		panel.remove(labelAddText[i]);
		panel.remove(label[i]);
		panel.remove(labelDate[i]);
		contentPane.updateUI();
		panel.updateUI();

		try
		{
			for(int k = 0; k <label.length; k++)
			{	
				Container parentLbl = labelDate[k].getParent();
				Container parentDate = label[k].getParent();
				Container parentSep = separator[k].getParent();
				Container parentAddText = labelAddText[k].getParent();

				
				parentSep.remove(separator[i]);
				parentLbl.remove(label[i]);
				parentDate.remove(labelDate[i]);
				parentAddText.remove(labelAddText[i]);
				panel.remove(separator[i]);
				panel.remove(labelAddText[i]);

				notizenList.remove(i);  //remove from list
				contentPane.updateUI();
				
				System.out.println(i);
				
				save();
				
			}

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public void checkForNotes(File file, boolean cloud)
	{
		
		 try {
	          if(fileLocalNotes.exists() && fileLocalNotes.length() > 0 && fileCloudNotes.exists() && !cloud){    
	        	  
	        	  readJSON(file);
	        	  
	        	  for(int i = 0; i < notizenList.size(); i++)
	        	  {
	        		  try {
	        			  updateLabel(i);  //sets preview text
		        		  updateLabelAddText(i);
	        		  	} catch (Exception e) {}		
	        		 
	        	  }
	        	
	            }else if(cloud) {
	            	URL url = new URL("file:///C:/Users/Kirstein/Desktop/Website/Own/notes/note.html");	//http://raspberrypi/Own/downloads/note.html
	            	
	            	URLConnection connec = url.openConnection();
	            	BufferedReader bf = new BufferedReader(new InputStreamReader(connec.getInputStream()));
	            	
	            	
	            	Document doc;
	            	doc = Jsoup.connect("file:///C:/Users/Kirstein/Desktop/Website/Own/notes/note.html").get();
	            	
	            	Elements el = doc.getElementsByClass("text");
	            	System.out.println(el.size());
	            	
	            	
	            	while((bf.readLine()) != null) {
	            		cloudJson += bf.readLine();
	            	}
	            	
	            	System.out.println(cloudJson);
	            	
	            }else {
	            	fileLocalNotes.createNewFile();
	            	fileCloudNotes.createNewFile();
	            }
	            	
	        } catch (ArrayIndexOutOfBoundsException | IOException e) {         
	            e.printStackTrace();
	        }                

	   }
	  
	
	public void save()
	{
		 try {
	            if(!fileLocalNotes.exists()) {
	            	fileLocalNotes.createNewFile();
	            	System.out.println("file created in: " + fileLocalNotes.getAbsolutePath());
	            }

	          writeJSON(fileLocalNotes);
	            

	        } catch (IOException e) { e.printStackTrace(); }     
	}
	
	public void addHorizontalLine(int i)
	{
		separator[i] = new JSeparator();
		separator[i].setBounds(0, (i * 40) + 35, 361, 25);
		separator[i].setForeground(Color.GRAY);
		panelContactPanel.add(separator[i]);
	}
	
	public void addAdditionalText(String text, int i)
	{
		labelAddText[i] = new JLabel(text);
		labelAddText[i].setBounds(60, (i * 40) + 15, 200, 25);
		panelContactPanel.add(labelAddText[i]);
	}
	
	public void printList()
	{
		for(Notiz notiz : notizenList)
		{
			System.out.println("------------------------");
			System.out.println("Printing List Now");
			System.out.println(notiz.getText());
			System.out.println("Stopped Printing");
			System.out.println("------------------------");
		}
	}
	public void addDateLabel(int i)
	{
		labelDate[i] = new JLabel(notizenList.get(i).getDate());
		labelDate[i].setBounds(0, (i * 40) + 15 , 100, 25);
		panelContactPanel.add(labelDate[i]);
	}
	
	public void addLabel(String text, int i)
	{
		label[i] = new JLabel(text);
		label[i].setBounds(0, i * 40, 350, 25);
		label[i].setFont(new Font("Ebrima", Font.BOLD, 15));

		panelContactPanel.add(label[i]);
		switchToNotesPanel(label[i], null, i);
		switchToNotesPanel2(label[i]);
		
		addDateLabel(i);
		addHorizontalLine(i);
		
		addAdditionalText("no additional text", i);

		System.out.println(i);
		
		System.out.println("NotizenList lenge: " + notizenList.size());
		
	
	}
	
	public void updateLabelAddText(int i)
	{
		System.out.println("updating Label Additional Text: " + i);
		
		String[] lines = notizenList.get(i).getText().split("\\n");
		for(int l = 1; l < lines.length ; l++)
		{
			try
			{
				if(lines[l] == null || (lines[l].isEmpty()) || (lines[l].length() < 2) || (lines[l].equalsIgnoreCase(null)))
					{
						labelAddText[i].setText("No additional text");

						System.out.println("there is no line");
					}else
						{
					
						
						System.out.println("checking line" + l);
						labelAddText[i].setText(lines[l]);
						System.out.println("text for line: " + l + " " + lines[l]);
						break;
						}
			}catch(ArrayIndexOutOfBoundsException e)
			{
				e.printStackTrace();
			}
		
		}
		
	}
	public void updateLabel(int i, String object)
	{
		//String text = notizenList.isEmpty() ? "" : notizenList.get(0).getText();
	
		if(object.length() == 0 || object == null || object.length() < 1) {
			label[i].setText("Neue Notiz");
		}else {
			label[i].setText((String) object);
		}
		
	}
	
	public void switchToNotesPanel2(JLabel website) {
        website.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
               
            	
            	if(i != 0)i--;
            	
        	
            	panelContactPanel.updateUI();
				panel.updateUI();

            	
            }
            @Override
            public void mouseExited(MouseEvent arg0) {
            	super.mouseExited(arg0);
            	website.setForeground(Color.BLACK);
            }
            
            @Override
            public void mouseEntered(MouseEvent arg0) {
            	super.mouseEntered(arg0);
            	website.setForeground(new Color(100, 100, 100));
            }
        });
	}
	
	public void switchToNotesPanel(JLabel website, String what, int i) {
        website.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
               addFakePanelComps();
               startPanel.setVisible(false);
            	besonderesI = i;
            	
        		lblDate.setText(notizenList.get(i).getDate() + " --- " +notizenList.get(i).getTime());
        		
            	System.out.println("notesTextArea2.setText(notizenList.get("+i+").getText()");
            	notesTextArea2.setText(notizenList.get(i).getText());
            	notesTextArea2.setFont(new Font(notesTextArea2.getFont().getFontName(), 0, notizenList.get(i).getSize()));
            	//System.out.println("FO>NT:__::::::::::::::: + " + notesTextArea2.getFont().getSize() + " " +notesTextArea2.getFont().getStyle() + " " +  notesTextArea2.getFont().getFontName());
				panel.updateUI();
            	panelContactPanel.updateUI();

        	
            	new javax.swing.Timer(1, new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						fakePanel.setLocation(fakePanel.getX() - 2, 0);

						startPanel.updateUI();
						contentPane.updateUI();

						 System.out.println(fakePanel.getX());
		                  if (fakePanel.getLocation().getX() < 10) 
		                  {
				               startPanel.remove(lblNotizen);

		                	  addNotesPanelComps();
		                	  
		                     ((javax.swing.Timer) e.getSource()).stop();
		                     System.out.println("Timer stopped");
		             		cl.show(contentPane,"notizenPanel");

		                     fakePanel.setBounds(400, 0, 361, 505);

			            }	
							
					}
				}).start();
            	
            	
            }
            @Override
            public void mouseExited(MouseEvent arg0) {
            	super.mouseExited(arg0);
            	website.setForeground(Color.BLACK);
            }
            
            @Override
            public void mouseEntered(MouseEvent arg0) {
            	super.mouseEntered(arg0);
            	website.setForeground(new Color(85, 85, 85));
            }
        });
	}

	
	public void switchToStartPanel2(JLabel website, String what, int i) {
        website.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
               
            		System.out.println("switchToStartPanel I ist: " + i);
            		
            		updateLabel(i);
            		

					panel.updateUI();            	
            }
        });
	}
	
	public void updateLabel(int i)
	{
		
		String[] lines = notizenList.get(i).getText().split("\\n");
		updateLabel(i, lines[0]);
		
	}
	
	
	public void switchToStartPanel(JLabel website, String what) {
        website.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
               
                fakePanel.setBounds(0, 0, 361, 505);
                addFakePanelComps();
        			startPanel.add(lblNotizen);

					panel.updateUI();
            		i++;
            		new javax.swing.Timer(1, new ActionListener() {
    					
    					@Override
    					public void actionPerformed(ActionEvent e) {
    						fakePanel.setLocation(fakePanel.getX() + 2, 0);
    				
    						startPanel.updateUI();
    						contentPane.updateUI();

    						 System.out.println(fakePanel.getX());
    		                  if (fakePanel.getLocation().getX() > 361) 
    		                  {
    		                	  addNotesPanelComps();
    		                	  
    		                     ((javax.swing.Timer) e.getSource()).stop();
    		                     System.out.println("Timer stopped");

    			            }	
    							
    					}
    				}).start();
  		
            		cl.show(contentPane, "startPanel");
            }
            @Override
            public void mouseExited(MouseEvent arg0) {
            	super.mouseExited(arg0);
            	website.setForeground(new Color(255, 140, 0));
            	lblOrangeBackIcon.setIcon(orangeBackIcon);
            	panel.updateUI();

            }
            
            @Override
            public void mouseEntered(MouseEvent arg0) {
            	super.mouseEntered(arg0);
            	website.setForeground(new Color(255, 200, 0));
            	lblOrangeBackIcon.setIcon(yellowBackIcon);
            	panel.updateUI();
            	
           
            }
        });
	}
	
	
	public void setDefaultLook()
	{
		try {
			  UIManager.setLookAndFeel(defaultLook);
		} catch (UnsupportedLookAndFeelException e2) {
			e2.printStackTrace();
		}
	}
	
	public void setWindowsLook()
	{
		try {
			  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e2) {
			e2.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void writeJSON(File file)
	{
		try
		{
		
            BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsolutePath()));

		 for(int l = 0; l < notizenList.size(); l++)
         {
         	
         
		JSONObject outside = new JSONObject();
		
		JSONObject map = new JSONObject();
		JSONObject infos = new JSONObject();

		
		
		outside.put("map", map);
		map.put("infos", infos);
		
		
		infos.put("Index", l);
		infos.put("Date", notizenList.get(l).getDate());
		infos.put("Time", notizenList.get(l).getTime());
		infos.put("Text", notizenList.get(l).getText());
		infos.put("Size", notizenList.get(l).getSize());
		infos.put("Color", notizenList.get(l).getColor());

		
		System.out.println(outside);
		
		
	    bw.write("[" + outside.toJSONString() + "]");
	    bw.newLine();
	          

         }
	            
	   
		  bw.close();
		 
		} catch (IOException e) { 
	            e.printStackTrace(); 
	            
	     } 
		 
         


	}
	
	public void clearNotesList() {
		notizenList.clear();
	}
	
	public void readJSON(File file)
	{
		final  JSONParser jsonParser = new JSONParser();
		JSONArray array = null;
		
		
		try
		{
			FileReader fr = new FileReader(file);
			BufferedReader in = new BufferedReader(fr);
		       
			try
			{
				String inputLine;
		        while ((inputLine = in.readLine()) != null) {
		            array = (JSONArray) jsonParser.parse(inputLine);
		            System.out.println(inputLine);
		            
		            JSONObject jsonProfile = (JSONObject) array.get(0);
				      
			        JSONObject map = (JSONObject) jsonProfile.get("map");
			        JSONObject infos = (JSONObject) map.get("infos");
			        
			        long index = (Long) infos.get("Index");
			        String time = (String) infos.get("Time");
			        String text = (String) infos.get("Text");
			        String date = (String) infos.get("Date");
			        long size = (long) infos.get("Size");
			        String color = (String) infos.get("Color");

			        System.out.println("Index: " + index);
			        System.out.println("Time: " + time);
			        System.out.println("Text: " + text);
			        System.out.println("Date: " + date);
			        System.out.println("Size: " + date);
			        System.out.println("Color: " + date);

			        
			        notizenList.add(new Notiz(text, date, time, i, (int) size, color));
					addLabel(notizenList.get((int) index).getText(), (int) index);
					lblAnzahlNotizen.setText(notizenList.size() + " Notizen");
					

					
					addHorizontalLine((int) index);
					
					
					panel.updateUI();
		        }
		        in.close();
			}catch(Exception e)
			{
				setWindowsLook();
				e.printStackTrace();
				JOptionPane.showMessageDialog(SwingUtilities.getRootPane(contentPane), "Couldn't read Infos");
				setDefaultLook();
			}
			
		  
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void changeColorOfPane(JTextPane textPane, Color c)
	{
		StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = textPane.getDocument().getLength();
        textPane.setCaretPosition(len);
        textPane.setCharacterAttributes(aset, false);
	}
	
	public static void appendToPane(JTextPane textPane, String msg, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = textPane.getDocument().getLength();
        textPane.setCaretPosition(len);
        textPane.setCharacterAttributes(aset, false);
        textPane.replaceSelection(msg + System.lineSeparator());
    }
	
	public void settingsAnimation()
	{
		System.out.println("Settings Panel");
		new javax.swing.Timer(1, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if((panel_Settings.getLocation().getY() > 340) && extended_Settings == false)
				{
					panel_Settings.setSize(panel_Settings.getWidth(), panel_Settings.getHeight() + 1);
					panel_Settings.setLocation(panel_Settings.getX(), panel_Settings.getY() -1);
					
					
					 if(panel_Settings.getLocation().getY() == 341)
                	 {
						 extended_Settings = true;
                		 ((javax.swing.Timer) e.getSource()).stop();
		                   System.out.println("Timer stopped");
		                   System.out.println("true");
		                   
		   				initSettingsPanel();

                	 }
					
				}else if(panel_Settings.getLocation().getY() < 439 && extended_Settings == true){ 
                	 
                	  
                	  
                	  panel_Settings.setSize(panel_Settings.getWidth(), panel_Settings.getHeight() - 1);
	                	
                	  panel_Settings.setLocation(panel_Settings.getX(), panel_Settings.getY() + 1);
	                	 
                	  
                	  if(panel_Settings.getLocation().getY() == 439)
	                	 {
                		  		extended_Settings = false;
	                		 ((javax.swing.Timer) e.getSource()).stop();
			                   System.out.println("Timer stopped");
			                   System.out.println("false");
	                	 }
                  }

			}
		}).start();
	}
}
