package Notes;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class NotesApp extends JFrame{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NotesApp frame = new NotesApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	 NotesApp() {
		
		setTitle("Notes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		add(new Mechanics().contentPane);
		setIconImage(new ImageIcon(NotesApp.class.getResource("/pictures/notizIcon32x.png")).getImage());
	   
		setResizable(false);

		pack();
		setLocationRelativeTo(null);
		

		
	}
	
	

}
