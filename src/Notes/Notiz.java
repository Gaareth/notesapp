package Notes;

public class Notiz {
	
	private String text;
	private String date;
	private String time;
	private int order;
	private int size;
	private String color;

	
	public Notiz(String text, String date, String time, int order, int size, String color) {
		this.text = text;
		this.date = date;
		this.order = order;
		this.time = time;
		this.size = size;
		this.color = color;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	

}
